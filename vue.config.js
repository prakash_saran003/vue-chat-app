module.exports = {
  devServer: {
    proxy: {
      "^/api": {
        target: "http://localhost:3080",
        changeOrigin: true,
        // secure: false,
        // pathRewrite: { "^/api": "/api" },
        // logLevel: "debug",
      },
    },
  },
  publicPath: "",
  indexPath: "../views/index.html",
  outputDir: "../chat-app/public",
  assetsDir: "",
};
