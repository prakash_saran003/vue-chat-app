import { createApp } from "vue";
import Toast from "vue-toastification";
import App from "./App.vue";
import "vue-toastification/dist/index.css";

const app = createApp(App);
const options = {
  // You can set your default options here
  transition: "Vue-Toastification__bounce",
  maxToasts: 30,
  newestOnTop: true,
};

app.use(Toast, options);
app.mount("#app");
