export async function getAllUsers() {
  const response = await fetch("/api/users");
  return await response.json();
}

export async function createUser(data) {
  const response = await fetch(`/api/user-signup`, {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({ user: data }),
  });
  return await response.json();
}

export async function signIn(data) {
  const response = await fetch(`/api/user-signin`, {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({ user: data }),
  });
  return await response.json();
}

export async function signOut(data) {
  const response = await fetch(`/api/user-signout`, {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({ user: data }),
  });
  return await response.json();
}

export async function checkSessionPresent() {
  const response = await fetch(`/api/user-loggedIn`);
  return await response.json();
}

export async function getUserMsg(userId, fetchNo) {
  const response = await fetch(`/api/chat-messages/` + userId + "/" + fetchNo);
  return await response.json();
}

export async function sendMessage(data) {
  const response = await fetch(`/api/send-user-message`, {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({ message: data }),
  });
  return await response.json();
}

export async function updateProfilePic(data) {
  const response = await fetch(`/api/update-profile-pic`, {
    method: "POST",
    headers: { enctype: "multipart/form-data" },
    body: data,
  });
  return await response.json();
}

export async function forgetPassword(data) {
  const response = await fetch(`/api/forget-password`, {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({ user: data }),
  });
  return await response.json();
}
